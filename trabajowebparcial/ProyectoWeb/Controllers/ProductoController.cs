﻿using Business;
using Business.Implementacion;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoWeb.Controllers
{
    public class ProductoController : Controller
    {

        private IProductoService productoservice = new ProductoService();
        private ITipoProductoService tproductoservice = new TipoProductoService();

        // GET: Producto
        public ActionResult Index()
        {
            return View(productoservice.readAll());
        }

        // GET: Producto/Details/5
        public ActionResult Details(int id)
        {
            Producto pro = new Producto()
            {
                IdProducto = id
            };
            return View(productoservice.findForId(pro));
        }

        // GET: Producto/Create
        public ActionResult Create()
        {
            Producto pro = new Producto()
            {
                TipoProductoList = new SelectList(tproductoservice.readAll(), "IdTipoProducto", "Descripcion")
            };

            return View(pro);
        }

        // POST: Producto/Create
        [HttpPost]
        public ActionResult Create(Producto pro)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    productoservice.create(pro);
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Producto/Edit/5
        public ActionResult Edit(int id)
        {
            Producto pro = new Producto()
            {
                
                IdProducto = id
            };
          
            return View(productoservice.findForId(pro));
        }

        // POST: Producto/Edit/5
        [HttpPost]
        public ActionResult Edit(Producto pro)
        {

            // TODO: Add update logic here
            if (ModelState.IsValid)
            {
                productoservice.update(pro);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        // GET: Producto/Delete/5
        public ActionResult Delete(int id)
        {
            Producto pro = new Producto();
            pro.IdProducto = id;
            return View(productoservice.findForId(pro));
        }

        // POST: Producto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Producto pro = new Producto();
                pro.IdProducto = id;
                if (ModelState.IsValid)
                {
                    productoservice.eliminar(pro);
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
