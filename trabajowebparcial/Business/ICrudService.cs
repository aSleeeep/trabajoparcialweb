﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public interface ICrudService<T>
    {
        void create(T t);
        void update(T t);
        void eliminar(T t);


        T findForId(T t);
        List<T> readAll();
    }
}
