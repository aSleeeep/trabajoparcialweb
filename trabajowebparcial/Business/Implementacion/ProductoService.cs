﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using Data;
using Data.Implementacion;

namespace Business.Implementacion
{
    public class ProductoService : IProductoService
    {
        private IClienteRepository pr = new ProductoRepository();

        public void create(Producto t)
        {
            pr.create(t);
        }

        public void eliminar(Producto t)
        {
            pr.eliminar(t);
        }

        public Producto findForId(Producto id)
        {

            return pr.findForId(id);
        }

        public List<Producto> readAll()
        {
            return pr.readAll();
        }

        public void update(Producto t)
        {
            pr.update(t);
        }
    }
}
