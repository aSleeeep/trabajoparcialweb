﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Data.Implementacion
{
    public class ProductoRepository : IClienteRepository
    {

        #region findForId
        public Producto findForId(Producto t)
        {
            Producto pro = null;

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["TP_Web"].ToString()))
            {
                con.Open();
                var cmd = new SqlCommand("usp_producto_buscar", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdProducto", t.IdProducto);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    pro = new Producto()
                    {
                        IdProducto = Convert.ToInt32(dr[0]),
                        Nombre = dr[1].ToString(),
                        Descripcion = dr[2].ToString(),
                        Precio = Convert.ToDecimal(dr[3]),
                        IdTipoProducto = Convert.ToInt32(dr[4])
                    };
                }
                dr.Close();
            }
            return pro;
        }

        #endregion

        #region Create
        public void create(Producto t)
        {

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["TP_Web"].ToString()))
            {
                con.Open();
                var cmd = new SqlCommand
  ("usp_producto_crear", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Nombre", t.Nombre);
                cmd.Parameters.AddWithValue("@Descripcion", t.Descripcion);
                cmd.Parameters.AddWithValue("@Precio", t.Precio);
                cmd.Parameters.AddWithValue("@IdTipoProducto", t.IdTipoProducto);
                cmd.Parameters.Add("@IdProducto", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                t.IdProducto = (int)cmd.Parameters["@IdProducto"].Value;
            }
        }
        #endregion

        #region readAll

        public List<Producto> readAll()
        {
            List<Producto> lproductos = new List<Producto>();

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["TP_Web"].ToString()))
            {
                con.Open();
                var cmd = new SqlCommand("usp_producto_listar", con);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();
                Producto pro;
                while (dr.Read())
                {
                    pro = new Producto()
                    {
                        IdProducto = Convert.ToInt32(dr[0]),
                        Nombre = dr[1].ToString(),
                        Descripcion = dr[2].ToString(),
                        Precio = Convert.ToDecimal(dr[3]),
                        IdTipoProducto = Convert.ToInt32(dr[4])
                    };
                    lproductos.Add(pro);
                }
                dr.Close();
            }
            return lproductos;
        }
        #endregion

        #region update 

        public void update(Producto t)
        {

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["TP_Web"].ToString()))
            {
                con.Open();
                var cmd = new SqlCommand("usp_producto_actualizar", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Nombre", t.Nombre);
                cmd.Parameters.AddWithValue("@Descripcion", t.Descripcion);
                cmd.Parameters.AddWithValue("@Precio", t.Precio);
                cmd.Parameters.AddWithValue("@IdTipoProducto", t.IdTipoProducto);
                cmd.Parameters.AddWithValue("@IdProducto", t.IdProducto);

                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region delete 

        public void eliminar(Producto t)
        {

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["TP_Web"].ToString()))
            {
                con.Open();

                var cmd = new SqlCommand("usp_producto_eliminar", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdProducto", t.IdProducto);

                cmd.ExecuteNonQuery();

            }
        }
        #endregion


    }
}
