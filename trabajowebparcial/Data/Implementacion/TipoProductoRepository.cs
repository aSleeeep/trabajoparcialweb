﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Data.Implementacion
{
    public class TipoProductoRepository : ITipoProductoRepository
    {
        public void create(TipoProducto t)
        {
            throw new NotImplementedException();
        }

        public void eliminar(TipoProducto t)
        {
            throw new NotImplementedException();
        }

        public TipoProducto findForId(TipoProducto t)
        {
            throw new NotImplementedException();
        }

        public List<TipoProducto> readAll()
        {
            List<TipoProducto> ltproductos = new List<TipoProducto>();

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["TP_Web"].ToString()))
            {
                con.Open();
                var cmd = new SqlCommand("usp_tproducto_listar", con);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();
                TipoProducto tpro;
                while (dr.Read())
                {
                    tpro = new TipoProducto()
                    {
                        IdTipoProducto = Convert.ToInt32(dr[0]),
                        Descripcion = dr[1].ToString()

                    };
                    ltproductos.Add(tpro);
                }
                dr.Close();
            }
            return ltproductos;
        }


        public void update(TipoProducto t)
        {
            throw new NotImplementedException();
        }
    }
}
