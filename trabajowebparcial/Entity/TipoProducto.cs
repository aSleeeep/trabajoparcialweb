﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TipoProducto
    {
        [DisplayName("Id. Producto")]
        public int IdTipoProducto { get; set; }

        [DisplayName("Descripcion")]
        public string Descripcion { get; set; }
    }
}
