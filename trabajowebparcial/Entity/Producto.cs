﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Entity
{
    public class Producto
    {
        public Producto()
        {
            this.TipoProductoList = new SelectList(new List<string>());
        }

        [DisplayName("Id. Producto")]
        public int IdProducto { get; set; }

        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Descripcion")]
        public string Descripcion { get; set; }

        [DisplayName("Precio")]
        public decimal Precio { get; set; }

        [DisplayName("Tipo Producto")]
        public int IdTipoProducto { get; set; }

        public SelectList TipoProductoList { get; set; }
    }
}
